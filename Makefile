llw = 10
lastCol=11
di=200
dj=200
generate = 1
w_coeffs = 4
epsilon_m0=0.00001
epsilon=0.00000000001
li=200
lj=200
lw=5
llw=10
size=100
test=0
si=200
sj=200
l1=0.2
l2=0.3
l3=1
pGetter0=9
qGetter0=10
pGetter=9
qGetter=10
testcoeff1=1 
testcoeff2=1
testcoeff3=0.2
#testcoeff1=0.5 
#testcoeff2=1
#testcoeff3=1
tileWidth=400
tileHeight=400

all: 
	g++ -ggdb -o bin/sfs src/sfslib.cpp src/point6d.cpp src/point14d.cpp src/sfs.cpp src/output.cpp src/main.cpp -L/usr/local/lib -L/usr/lib/x86_64-linux-gnu/ -I. -I/usr/include/opencv4/ -I/usr/include/pcl-1.10 -I/usr/include/eigen3/ -I/usr/local/include/ginac -lboost_system -lboost_iostreams -lpcl_visualization -lpcl_io -lpcl_common -lgsl -lgslcblas -lginac -lopencv_imgproc -lopencv_core -lopencv_imgcodecs
run:
	./bin/sfs pcds/S.jpeg  -n_coeffs=$(n_coeffs) -lastCol=$(lastCol) -di=$(di) -dj=$(dj) -generated_derivatives=$(generate) -w_coeffs=$(w_coeffs) -epsilon_m0=$(epsilon_m0) -epsilon=$(epsilon) -li=$(li) -lj=$(lj) -lw=$(lw) -llw=$(llw) -size=$(size) -test=$(test) -si=$(si) -sj=$(sj) -l1=$(l1) -l2=$(l2) -l3=$(l3) -testcoeff1=$(testcoeff1) -testcoeff2=$(testcoeff2) -testcoeff3=$(testcoeff3) -pGetter0=$(pGetter0) -qGetter0=$(qGetter0) -pGetter=$(pGetter) -qGetter=$(qGetter) -tileWidth=$(tileWidth) -tileHeight=$(tileHeight) > tt
